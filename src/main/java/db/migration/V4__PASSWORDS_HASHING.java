package db.migration;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class V4__PASSWORDS_HASHING extends BaseJavaMigration {
    @Override
    public void migrate(Context context) throws Exception {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        Connection connection = context.getConnection();
        Statement statement = connection.createStatement();
        ResultSet usersData = statement.executeQuery("select login, password from \"user\"");

        final PreparedStatement userUpdated = connection.prepareStatement("update \"user\" set password = ? where login = ?;");

        while (usersData.next()) {
            String login = usersData.getString("login");
            String password = usersData.getString("password");

            String passwordHashed = passwordEncoder.encode(password);

            userUpdated.setString(1, passwordHashed);
            userUpdated.setString(2, login);
            userUpdated.execute();  // update user with encrypted password
        }
        usersData.close();
    }
}
