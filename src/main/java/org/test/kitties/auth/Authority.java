package org.test.kitties.auth;

import org.springframework.security.core.GrantedAuthority;
import org.test.kitties.model.RoleModel;

public class Authority implements GrantedAuthority {
    private final RoleModel role;

    public Authority(RoleModel role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role.getName();
    }
}
