package org.test.kitties.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.test.kitties.db.entity.UserEntity;
import org.test.kitties.util.Role;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserModel implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String login;
    private String name;
    private String password;
    private Date created;
    private RoleModel role;

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(final Object other) {
        if(this == other)
            return true;

        if(! ( other instanceof UserModel) )
            return false;

        final UserModel otherUser = (UserModel) other;
        return Objects.equals(id, otherUser.id);
    }

    public static UserModel toModel(UserEntity userEntity) {
        UserModel user = new UserModel();
        user.setId(userEntity.getId());
        user.setLogin(userEntity.getLogin());
        user.setName(userEntity.getName());
        user.setPassword(userEntity.getPassword());
        user.setCreated(userEntity.getCreated());
        user.setRole(RoleModel.toModel(userEntity.getRole()));
        return user;
    }

    public static UserEntity toEntity(UserModel user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(user.getId());
        userEntity.setLogin(user.getLogin());
        userEntity.setName(user.getName());
        userEntity.setPassword(user.getPassword());
        userEntity.setCreated(user.getCreated());
        userEntity.setRole(RoleModel.toEntity(user.getRole()));
        return userEntity;
    }

    public boolean isAdmin() {
        return Role.Admin.getName().equals(getRole().getName());
    }
}
