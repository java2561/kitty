package org.test.kitties.model;

import lombok.Data;
import org.test.kitties.db.entity.RoleEntity;
import org.test.kitties.util.Role;

import java.io.Serializable;

@Data
public class RoleModel implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;

    public static RoleModel toModel(RoleEntity roleEntity) {
        RoleModel role = new RoleModel();
        role.setId(roleEntity.getId());
        role.setName(roleEntity.getName());
        return role;
    }

    public static RoleEntity toEntity(RoleModel role) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(role.getId());
        roleEntity.setName(role.getName());
        return roleEntity;
    }

    public boolean isAdmin() {
        return Role.Admin.equals(getName());
    }
}
