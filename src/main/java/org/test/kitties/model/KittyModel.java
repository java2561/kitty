package org.test.kitties.model;

import lombok.Data;
import org.test.kitties.db.entity.KittyEntity;

import java.util.Objects;

@Data
public class KittyModel {
    private Long id;
    private String name;
    private String imgUrl;
    private Long rating;

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(final Object other) {
        if(this == other)
            return true;

        if(! ( other instanceof KittyModel) )
            return false;

        final KittyModel otherKitty = (KittyModel) other;
        return Objects.equals(id, otherKitty.id);
    }

    public void increaseRating() {
        if(rating < Long.MAX_VALUE)
            rating++;
    }

    public static KittyModel toModel(KittyEntity kittyEntity) {
        KittyModel kitty = new KittyModel();
        kitty.setId(kittyEntity.getId());
        kitty.setName(kittyEntity.getName());
        kitty.setImgUrl(kittyEntity.getImgUrl());
        kitty.setRating(kittyEntity.getRating());
        return kitty;
    }

    public KittyEntity toEntity() {
        KittyEntity kittyEntity = new KittyEntity();
        kittyEntity.setId(this.getId());
        kittyEntity.setName(this.getName());
        kittyEntity.setImgUrl(this.getImgUrl());
        kittyEntity.setRating(this.getRating());
        return kittyEntity;
    }
}
