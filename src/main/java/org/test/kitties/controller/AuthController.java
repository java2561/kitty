package org.test.kitties.controller;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.test.kitties.auth.CustomUserDetails;
import org.test.kitties.model.RoleModel;
import org.test.kitties.model.UserModel;
import org.test.kitties.service.RoleService;
import org.test.kitties.service.UserService;
import org.test.kitties.util.Role;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@Controller
@AllArgsConstructor
@Log4j2
public class AuthController {
    private final UserService userService;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    @GetMapping("/login")
    public String login(@ModelAttribute("error") String error, @ModelAttribute("isLogout") String isLogout,
                        @ModelAttribute("loginSignIn") String loginSignIn, Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication != null && authentication.isAuthenticated()) {  // user already logged in
            for(GrantedAuthority authority : authentication.getAuthorities()) {
                if(Role.all().contains(authority.getAuthority())) { // check if user has known role (he can have also ROLE_ANONYMOUS)
                    log.info("User is already logged in with role " + authority.getAuthority());
                    return "redirect:/";
                }
            }
        }

        model.addAttribute("authMessage", new AuthMessage("true".equalsIgnoreCase(isLogout), error));
        model.addAttribute("loginSignIn", loginSignIn);

        return "/user/login";
    }

    @PostMapping("/signup")
    public String registerAccount(HttpServletRequest request,
                                  @RequestParam(name="login") String login, @RequestParam(name="name") String name,
                                  @RequestParam(name="password") String password, Model model) {

        List<String> errors = new ArrayList<>();
        if(StringUtils.isEmpty(login))
            errors.add("Поле 'Логин' должно быть не пустым");
        if(StringUtils.isEmpty(name))
            errors.add("Поле 'Имя' должно быть не пустым");
        if(StringUtils.isEmpty(password))
            errors.add("Поле 'Пароль' должно быть не пустым");

        UserModel userSameLogin = userService.findByLogin(login);
        if (userSameLogin != null)
            errors.add(String.format("Пользователь с логином '%s' уже существует", login));

        if(errors.isEmpty()) {
            RoleModel role = roleService.findByName(Role.User.getName());  // default role is "user", can be changed to admin in users control page
            UserModel user = new UserModel(-1L, login, name, passwordEncoder.encode(password), new Date(), role);

            userService.create(user);

            setUserAuthenticated(user, request); // successful login

            return "redirect:/";

        } else {
            String errorMessage = String.join("\r\n", errors);
            model.addAttribute("authMessage", new AuthMessage(errorMessage));

            model.addAttribute("loginSignUp", login);  // todo: XSS check
            model.addAttribute("nameSignUp", name);    // todo: XSS check
            return "/user/login";
        }
    }

    @PostMapping("/signin")
    public RedirectView signIn(HttpServletRequest request,
                               @RequestParam(name="login") String login, @RequestParam(name="password") String password,
                               RedirectAttributes attributes) {

        UserModel user = userService.findByLogin(login);
        String errorMessage = null;
        if(user == null)
            errorMessage = "Такого пользователя не существует";
        else {
            if(!passwordEncoder.matches(password, user.getPassword()))
                errorMessage = "Неверный пароль";
        }

        if(errorMessage != null) {
            attributes.addAttribute("error", errorMessage);
            attributes.addAttribute("loginSignIn", login);
            return new RedirectView("/login");
        } else {
            setUserAuthenticated(user, request); // successful login
            return new RedirectView("/");
        }
    }

    public static void setUserAuthenticated(UserModel user, HttpServletRequest request) {
        try {
            CustomUserDetails userDetails = new CustomUserDetails(user);
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
            SecurityContext context = SecurityContextHolder.getContext();
            context.setAuthentication(authentication);

            HttpSession session = request.getSession(true);
            session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, context);
        } catch (Exception exc) {
            SecurityContextHolder.getContext().setAuthentication(null);
            throw new AuthException("Не удалось войти");
        }
    }

    private static class AuthException extends RuntimeException {
        public AuthException(String message) {
            super(message);
        }
    }

    public static class AuthMessage {
        private String error;  public String getError() { return error; }
        private boolean logout;

        @SuppressWarnings("unused")
        public AuthMessage() { } // for empty default /login call

        public AuthMessage(String error) {
            this.error = error;
        }

        public AuthMessage(boolean logout, String error) {
            this.logout = logout;
            this.error = error;
        }

        public boolean hasError() { return error != null && ! StringUtils.isEmpty(error); }
        public boolean isLogout() { return logout; }
        public boolean showMessage() { return hasError() || isLogout(); }
    }
}
