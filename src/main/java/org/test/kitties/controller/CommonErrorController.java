package org.test.kitties.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@Log4j2
public class CommonErrorController implements ErrorController {
    private final ErrorAttributes errorAttributes;

    public CommonErrorController() {
        this.errorAttributes = new DefaultErrorAttributes();
    }

    @GetMapping("/*")      // all not mapped URLs
    public String unmappedURLs() {
        return "error/404";
    }

    @RequestMapping("/error")
    public String unhandledError(HttpServletRequest request, Model model) {
        Object statusCode = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        if (statusCode != null) {
            try {
                httpStatus = HttpStatus.valueOf((Integer)statusCode);
            } catch(Exception exc) {
                return error(request, model, httpStatus);
            }

            if(HttpStatus.NOT_FOUND.equals(httpStatus))
                return "error/404";
        }
        return error(request, model, httpStatus);
    }

    private String error(HttpServletRequest request, Model model, HttpStatus httpStatus) {
        Map<String, Object> mapErrorAttributes = getErrorAttributes(request);
        String strMessage = (String)mapErrorAttributes.get("message");

        model.addAttribute("httpStatus", httpStatus);
        model.addAttribute("message", strMessage);

        log.trace("Unhandled error: Http status: " + httpStatus.toString() + ". Message: " + strMessage);
        return "error/unhandled";
    }

    private Map<String, Object> getErrorAttributes(HttpServletRequest request) {
        WebRequest webRequest = new ServletWebRequest(request);
        return this.errorAttributes.getErrorAttributes(webRequest, ErrorAttributeOptions.of(ErrorAttributeOptions.Include.MESSAGE));
    }
}
