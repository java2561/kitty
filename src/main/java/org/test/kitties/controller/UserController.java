package org.test.kitties.controller;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.test.kitties.model.RoleModel;
import org.test.kitties.model.UserModel;
import org.test.kitties.service.RoleService;
import org.test.kitties.service.UserService;
import org.test.kitties.util.Role;
import org.test.kitties.util.UserLogoGenerator;
import org.test.kitties.util.UserValidator;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Controller
@AllArgsConstructor
@Log4j2
public class UserController {
    private final UserService userService;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    @GetMapping("/user")
    public String getUsers(@ModelAttribute("message") String message, Model model) {
        List<UserModel> users = userService.findAll();

        model.addAttribute("users", users);
        model.addAttribute("dateFormatter", new SimpleDateFormat("dd.MM.yyyy HH:mm"));
        model.addAttribute("logoGenerator", new UserLogoGenerator());

        model.addAttribute("loggedInUser", getLoggedInUser());

        if(! StringUtils.isEmpty(message)) {
            model.addAttribute("showMessage", true);
            model.addAttribute("message", message);
        }

        return "user/list";
    }

    @GetMapping("/user/{userId}")
    public String find(@PathVariable String userId, Model model) {
        UserValidator.validateUserId(userId);

        UserModel user = userService.findById(Long.valueOf(userId));

        UserValidator.validateUserExists(user, userId);

        List<RoleModel> roles = roleService.findAll();

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentRole = getAuthorityAsSingleObject(authentication.getAuthorities()).getAuthority();
        boolean isUserAdmin = Role.Admin.getName().equals(currentRole);

        model.addAttribute("dateFormatter", new SimpleDateFormat("dd.MM.yyyy HH:mm"));
        model.addAttribute("user", user);
        model.addAttribute("roles", roles);
        model.addAttribute("isUserAdmin", isUserAdmin);

        return "user/edit";
    }

    @DeleteMapping(value="/user/{userId}")
    public ResponseEntity<Object> delete(@PathVariable String userId) {
        UserValidator.validateUserId(userId);

        Long idUser = Long.valueOf(userId);
        UserModel user = userService.findById(idUser);

        List<UserModel> allUsers = userService.findAll();

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentRole = getAuthorityAsSingleObject(authentication.getAuthorities()).getAuthority();

        UserValidator.validateUserIsAdmin(currentRole);
        UserValidator.validateUserExists(user, userId);
        UserValidator.validateSelfDelete(user, authentication.getName());
        UserValidator.validateDeleteAdmin(user, allUsers);

        userService.delete(idUser);

        return ResponseEntity.noContent().build();
    }

    @PostMapping("/user/{userId}")
    public String save(@PathVariable(name="userId") String userId, @ModelAttribute(name="login") String login,
                       @ModelAttribute(name="name") String name, @ModelAttribute(name="password") String password,
                       @ModelAttribute(name="roleId") String roleId, Model model, HttpServletRequest request) {

        List<String> errors = new ArrayList<>();
        if(StringUtils.isEmpty(userId))
            errors.add("Поле 'userId' пустое");
        Long idUser = Long.valueOf(userId);
        boolean isNew = idUser.equals(-1L);

        if(StringUtils.isEmpty(login) && isNew)     // existing user don't change a login
            errors.add("Поле 'login' пустое");
        if(StringUtils.isEmpty(name))
            errors.add("Поле 'name' пустое");
        if(StringUtils.isEmpty(password) && isNew)  // new user must have password
            errors.add("Поле 'password' пустое");
        if(StringUtils.isEmpty(roleId))
            errors.add("Поле 'roleId' пустое");
        Long idRole = Long.valueOf(roleId);

        UserModel user = new UserModel(idUser, login, name, "", new Date(), null);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentRole = getAuthorityAsSingleObject(authentication.getAuthorities()).getAuthority();

        RoleModel role = roleService.findById(idRole);

        if(role == null) {
            errors.add(String.format("Выбранная роль с id = %s не существует", idRole));
            role = roleService.findByName(Role.User.getName());
            user.setRole(role);
        }  else {
            user.setRole(role);
            UserModel userExisting = userService.findById(idUser);

            // left the password the same if empty
            String passwordEncoded = StringUtils.isEmpty(password) ? userExisting.getPassword() : passwordEncoder.encode(password);
            login = isNew ? login : userExisting.getLogin();  // don't change user's login
            user = new UserModel(idUser, login, name, passwordEncoded, new Date(), role);

            if (idUser.equals(-1L)) { // create new
                if (userExisting != null)
                    errors.add(String.format("Пользователь с логином '%s' уже существует", login));
                else
                    userService.create(user);
            } else {                 // update existing
                try {
                    UserValidator.validateSelfEscalate(user, currentRole, authentication.getName());

                    List<UserModel> allUsers = userService.findAll();
                    UserValidator.validateSelfDeescalate(user, currentRole, allUsers);

                    userService.update(idUser, user);
                    if(user.getLogin().equals(authentication.getName()))  // edited self => re-login
                        AuthController.setUserAuthenticated(user, request);

                } catch(UserValidator.UserException exc) {
                    errors.add(exc.getMessage());
                }
            }
        }

        boolean isUserAdmin = Role.Admin.getName().equals(currentRole);

        if(! errors.isEmpty()) {
            List<RoleModel> roles = roleService.findAll();


            model.addAttribute("error", String.join("\n", errors));
            model.addAttribute("dateFormatter", new SimpleDateFormat("dd.MM.yyyy HH:mm"));
            model.addAttribute("user", user);
            model.addAttribute("roles", roles);
            model.addAttribute("isUserAdmin", isUserAdmin);
            return "user/edit";
        }

        return isUserAdmin ? "redirect:/user" : "redirect:/";
    }

    public GrantedAuthority getAuthorityAsSingleObject(Collection<? extends GrantedAuthority> authorities) {
        return authorities.iterator().next();
    }

    private UserModel getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        return userService.findByLogin(login);
    }
}
