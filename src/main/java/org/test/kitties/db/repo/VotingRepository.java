package org.test.kitties.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.test.kitties.db.entity.VotingEntity;

public interface VotingRepository extends JpaRepository<VotingEntity, Long> {
    VotingEntity findBySessionId(String sessionId);
}
