package org.test.kitties.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.test.kitties.db.entity.RoleEntity;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    RoleEntity findByName(String name);
}
