package org.test.kitties.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.test.kitties.db.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByLogin(String login);
}
