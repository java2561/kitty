package org.test.kitties.service;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.test.kitties.db.entity.UserEntity;
import org.test.kitties.db.repo.UserRepository;
import org.test.kitties.model.UserModel;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultUserService implements UserService {
    private final UserRepository userRepository;

    @NonNull @Override
    public UserModel create(@NonNull UserModel user) {
        UserEntity userEntity = userRepository.save(UserModel.toEntity(user));
        return UserModel.toModel(userEntity);
    }

    @Nullable @Override
    public UserModel update(Long userId, @NonNull UserModel user) {
        if(userId == null)
            return null;

        UserEntity userEntity = userRepository.findById(userId).orElse(null);
        if (userEntity == null)
            return null;

        Date createdOriginal = userEntity.getCreated();

        userEntity = UserModel.toEntity(user);
        userEntity.setId(userId);
        userEntity.setCreated(createdOriginal);  // don't change original creation date

        userEntity = userRepository.save(userEntity);
        return UserModel.toModel(userEntity);
    }

    @Override
    public boolean delete(Long userId) {
        if(userId == null)
            return false;

        if(! userRepository.existsById(userId))
            return false;

        userRepository.deleteById(userId);
        return true;
    }

    @NonNull @Override
    @Transactional(readOnly = true)
    public List<UserModel> findAll() {
        return userRepository.findAll()
            .stream()
            .map(UserModel::toModel)
            .collect(Collectors.toList());
    }

    @Nullable @Override
    public UserModel findById(Long userId) {
        if(userId == null)
            return null;

        UserEntity userEntity = userRepository.findById(userId).orElse(null);
        if(userEntity == null)
            return null;

        return UserModel.toModel(userEntity);
    }

    @Nullable @Override
    public UserModel findByLogin(String login) {
        if(login == null)
            return null;

        UserEntity userEntity = userRepository.findByLogin(login);
        if(userEntity == null)
            return null;

        return UserModel.toModel(userEntity);
    }
}
