package org.test.kitties.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.test.kitties.auth.CustomUserDetails;
import org.test.kitties.model.UserModel;

@Service
@RequiredArgsConstructor
public class DefaultUserDetailsService implements UserDetailsService {
    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserModel userEntity = userService.findByLogin(login);
        if (userEntity == null) {
            throw new UsernameNotFoundException(login);
        }
        return new CustomUserDetails(userEntity);
    }
}
