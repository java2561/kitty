package org.test.kitties.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.test.kitties.model.VotingModel;

public interface VotingService {
    @NonNull
    VotingModel create(@NonNull VotingModel voting);

    @Nullable
    VotingModel update(Long votingId, @NonNull VotingModel voting);

    @NonNull
    VotingModel getBySessionIdOrCreate(String sessionId);
}
