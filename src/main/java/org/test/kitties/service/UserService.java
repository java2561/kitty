package org.test.kitties.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.test.kitties.model.UserModel;

import java.util.List;

public interface UserService {
    @NonNull
    UserModel create(@NonNull UserModel user);

    @Nullable
    UserModel update(Long userId, @NonNull UserModel user);

    boolean delete(Long userId);

    @NonNull
    List<UserModel> findAll();

    @Nullable
    UserModel findById(Long userId);

    @Nullable
    UserModel findByLogin(String login);
}
