package org.test.kitties.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.test.kitties.model.RoleModel;

import java.util.List;

public interface RoleService {
    @Nullable
    RoleModel findById(Long roleId);

    @Nullable
    RoleModel findByName(String name);

    @NonNull
    List<RoleModel> findAll();
}
