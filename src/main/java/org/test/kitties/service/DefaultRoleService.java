package org.test.kitties.service;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.test.kitties.db.entity.RoleEntity;
import org.test.kitties.db.repo.RoleRepository;
import org.test.kitties.model.RoleModel;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultRoleService implements RoleService {
    private final RoleRepository roleRepository;

    @Nullable @Override
    public RoleModel findById(Long roleId) {
        if(roleId == null)
            return null;

        RoleEntity roleEntity = roleRepository.findById(roleId).orElse(null);
        if(roleEntity == null)
            return null;

        return RoleModel.toModel(roleEntity);
    }

    @Nullable @Override
    public RoleModel findByName(String name) {
        if(name == null)
            return null;

        RoleEntity roleEntity = roleRepository.findByName(name);
        if(roleEntity == null)
            return null;

        return RoleModel.toModel(roleEntity);
    }

    @NonNull @Override
    @Transactional(readOnly = true)
    public List<RoleModel> findAll() {
        return roleRepository.findAll()
            .stream()
            .map(RoleModel::toModel)
            .collect(Collectors.toList());
    }
}
