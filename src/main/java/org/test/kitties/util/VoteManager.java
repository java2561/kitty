package org.test.kitties.util;

import org.springframework.lang.NonNull;
import org.test.kitties.model.KittyModel;
import org.test.kitties.model.VotingModel;
import org.test.kitties.service.KittyService;
import org.test.kitties.service.VotingService;
import org.test.kitties.util.KittyPair;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.Set;

public class VoteManager {
    private final KittyService kittyService;
    private final VotingService votingService;
    private final VotingModel voting;

    private KittyModel kittyFor = null;
    private KittyPair pair = null;
    private boolean hasSomeError = false;
    private boolean alreadyVotedPair = false;
    private String errorMessage = null;

    public VoteManager(String forId, String againstId, HttpSession session,
                       KittyService kittyService, VotingService votingService) {

        this.kittyService = kittyService;
        this.votingService = votingService;

        voting = votingService.getBySessionIdOrCreate(session.getId());

        try {
            validateAndSetParams(forId, againstId);
        } catch (VoteException ve) {
            hasSomeError = true;
            errorMessage = ve.getMessage();
        }
    }

    private void validateAndSetParams(String forId, String againstId) {

        if (StringUtils.isEmpty(forId))
            throw new VoteException("'For' parameter not found");
        if(StringUtils.isEmpty(againstId))
            throw new VoteException("'Against' parameter not found");

        long idFor, idAgainst;

        try {
            idFor = Long.parseLong(forId);
            idAgainst = Long.parseLong(againstId);
        } catch (NumberFormatException nfe) {
            throw new VoteException("Votes 'for'/'against' parameters must contain ID values");
        }

        kittyFor = kittyService.findById(idFor);
        KittyModel kittyAgainst = kittyService.findById(idAgainst);

        if (kittyFor == null)
            throw new VoteException("Kitty " + idFor + " not found");

        if (kittyAgainst == null)
            throw new VoteException("Kitty " + idAgainst + " not found");

        pair = new KittyPair(kittyFor, kittyAgainst);
        if (voting.getViewedPairs().contains(pair)) {
            alreadyVotedPair = true;
            throw new VoteException("This kitties pair was already voted: " + kittyFor.getName() + " vs " + kittyAgainst.getName());
        }
    }

    @NonNull
    public VotingModel getVoting() {
        return voting;
    }

    public boolean hasSomeError() {
        return hasSomeError;
    }

    public boolean isAlreadyVotedPair() {
        return alreadyVotedPair;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public VotingModel vote() {
        if(hasSomeError)
            return null;

        kittyFor.increaseRating();
        kittyService.update(kittyFor.getId(), kittyFor);

        Set<KittyPair> setViewedPairs = new HashSet<>(voting.getViewedPairs());
        setViewedPairs.add(pair);
        voting.setViewedPairs(setViewedPairs);

        if(voting.getId() == null)
            return votingService.create(voting);
        else
            return votingService.update(voting.getId(), voting);
    }

    private static class VoteException extends RuntimeException {
        public VoteException(String message) {
            super(message);
        }
    }
}
