package org.test.kitties.util;

import org.test.kitties.model.UserModel;
import org.thymeleaf.util.StringUtils;

import java.util.List;

public class UserValidator {
    public static void validateUserId(String userId) {
        if (StringUtils.isEmpty(userId))
            throw new UserException("Не дайден параметр 'userId'");

        try {
            Long.valueOf(userId);
        } catch(NumberFormatException nfe) {
            throw new UserException("Параметр 'userId' - не число");
        }
    }

    public static void validateUserIsAdmin(String currentRole) {
        if(! Role.Admin.getName().equals(currentRole))
            throw new UserException("Пользователь с ролью user не может удалять других пользователей");
    }

    public static void validateUserExists(UserModel user, String userId) {
        if(user == null)
            throw new UserException("Пользователь с id = " + userId + " не найден");
    }

    public static void validateDeleteAdmin(UserModel user, List<UserModel> allUsers) {
        if(isUserOnlyAdmin(user.getRole().getName(), allUsers))
            throw new UserException("Нельзя удалить единственного пользователя с админской ролью");
    }

    public static void validateSelfDelete(UserModel user, String currentLogin) {
        if(user.getLogin().equals(currentLogin))
            throw new UserException("Нельзя удалить себя");
    }

    public static void validateSelfEscalate(UserModel user, String currentRole, String currentLogin) {
        if(user.getLogin().equals(currentLogin)) { // edit self
            //  user tries to become admin
            if (Role.User.getName().equals(currentRole) && Role.Admin.getName().equals(user.getRole().getName()))
                throw new UserException("Нельзя назначить себе роль админа. Это делает другой админ");
        }
    }

    public static void validateSelfDeescalate(UserModel user, String currentRole, List<UserModel> allUsers) {
        if(isUserOnlyAdmin(currentRole, allUsers)) {
            //  admin becomes user
            if (Role.Admin.getName().equals(currentRole) && Role.User.getName().equals(user.getRole().getName()))
                throw new UserException("Нельзя понизить себе роль. Должен быть хоть один админ");
        }
    }

    private static boolean isUserOnlyAdmin(String currentRole, List<UserModel> allUsers) {
        if(Role.Admin.getName().equals(currentRole)) {
            long admins = allUsers.stream().filter(UserModel::isAdmin).count();
            return admins == 1;
        }
        return false;
    }

    public static class UserException extends RuntimeException {
        public UserException(String message) {
            super(message);
        }
    }
}
