package org.test.kitties.util;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum Role {
    Admin("admin"),
    User("user");

    private String name;
    public String getName() {
        return name;
    }

    Role(String name) {
        this.name = name;
    }

    private static final Set<String> roleNames = Arrays.stream(values())
        .map(Role::getName)
        .collect(Collectors.toSet());

    public static Set<String> all() {
        return roleNames;
    }
}
