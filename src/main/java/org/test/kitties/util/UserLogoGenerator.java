package org.test.kitties.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.test.kitties.model.UserModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserLogoGenerator {
    public List<List<Point>> generate(UserModel user, int sizeInPixels, int points) {
        int pointSize = sizeInPixels / points;
        //int size = sizeInPixels / points;
        long seed = user.hashCode();
        Random random = new Random(seed);

        List<List<Point>> matrix = new ArrayList<>();
        for(int i = 0; i < points; i++) {
            List<Point> row = new ArrayList<>();
            int half = points / 2;

            for(int j = 0; j < half; j++) {
                double number = random.nextDouble();
                boolean render = number < 0.33333333;
                row.add(new Point(render,j * pointSize, i * pointSize, pointSize));
            }

            for(int j = half - 1; j >= 0; j--) {  // mirror half of row
                Point point = row.get(j);
                Point mirrorPoint = new Point(point.isRender(), points * pointSize - (j + 1) * pointSize, i * pointSize, pointSize);
                row.add(mirrorPoint);
            }
            matrix.add(row);
        }
        return matrix;
    }

    public List<Point> toFlatList(List<List<Point>> matrix) {
        List<Point> list = new ArrayList<>();
        for(List<Point> row: matrix)
            list.addAll(row);
        return list;
    }

    @Getter
    @AllArgsConstructor
    public static class Point {
        private boolean render;
        private int x;
        private int y;
        private int size;
    }
}
