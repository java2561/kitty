$(document).ready(function() {
    var deleteClicked = false;
    $('a#delete-link').on('click', function(e) {
        e.preventDefault();
        deleteClicked = true;
        const $deleteLink = $(this);
        const userId = $deleteLink.data('user-id');
        const username = $deleteLink.data('user-name');
        $.ajax({
            url: '/user/' + userId,
            type: 'DELETE',
            success: function() {
                window.location.href = '/user?message=Пользователь "' + username + ' удалён';
            }
        });
    } );
    $('.user.item').click(function() {
        if(deleteClicked)
            return;

        const $userItem = $(this);
        const userId = $userItem.data('user-id');
        window.location.href = '/user/' + userId;
    } );
} );