-- Initial DB data to start with

insert into kitty (name, rating, img_url) values ('Мурзик',   0, '/img/kitties/Murzik.jpg');
insert into kitty (name, rating, img_url) values ('Барсик',   0, '/img/kitties/Barsik.jpg');
insert into kitty (name, rating, img_url) values ('Муся',     0, '/img/kitties/Musya.jpg');
insert into kitty (name, rating, img_url) values ('Пушок',    0, '/img/kitties/Pushok.jpg');
insert into kitty (name, rating, img_url) values ('Бармалей', 0, '/img/kitties/Barmalei.jpg');

insert into role (name) values ('admin');
insert into role (name) values ('user');

-- Passwords are being encoded in BC-CRYPT in V4 migration

insert into "user" (login, name, password, created, role_id) (SELECT 'vasiliy', 'Василий Васильев',    '12345', now(), id FROM role WHERE name = 'user');
insert into "user" (login, name, password, created, role_id) (SELECT 'richard', 'Ричард Столлман',     '98765', now(), id FROM role WHERE name = 'user');
insert into "user" (login, name, password, created, role_id) (SELECT 'admin',  'Админ Админович', '54321', now(), id FROM role WHERE name = 'admin');