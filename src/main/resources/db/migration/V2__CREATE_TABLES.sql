-- Initial DB tables

create table kitty (
    id      bigserial primary key,
    img_url varchar(255) not null,
    name    varchar(255) not null,
    rating  bigint default 0
);
alter table kitty
    owner to kitties_db_user;

create table voting (
    id           bigserial primary key,
    session_id   varchar(255) not null constraint uk_voting_session_id unique,
    viewed_pairs varchar
);
alter table voting
    owner to kitties_db_user;

create table "role" (
    id   bigserial primary key,
    name varchar(255) not null constraint uk_role_name unique
);
alter table "role"
    owner to kitties_db_user;

create table "user" (
    id       bigserial primary key,
    login    varchar(255) not null constraint uk_user_login unique,
    name     varchar(255) not null,
    password varchar(255) not null,
    created  timestamp not null,
    role_id  int not null references role(id) on delete cascade
);
alter table "user"
    owner to kitties_db_user;