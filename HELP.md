# Инструкция

### Предварительная настройка окружения

Приложение использует JDK версии 11, СУБД PostgreSQL с версией >= 16.
Перед запуском необходимо создать базу данных в PostgreSQL со следующими параметрами:

**Пользователь**:
 * логин: kitties_db_user
 * пароль: 9Ui1jX4V5cSn10D

либо выполнить sql-скрипт: 

```
create role kitties_db_user login 
    password '9Ui1jX4V5cSn10D1'
    nosuperuser inherit nocreatedb nocreaterole noreplication;
```

**База данных**: kitties

**Порт подключения**: 5432

sql-скрипт создания БД:

```
create database kitties
    with owner = kitties_db_user
        encoding = 'UTF8'
        tablespace = pg_default
        template template0
        lc_collate = 'C'
        lc_ctype = 'English_United States.1252'
        connection limit = -1;
```

### Сборка и запуск

Осуществляется при помощи сборочной системы Gradle, проект можно открыть в IntelliJ Idea.

При первом запуске приложение создаёт необходимые таблицы в подготовленной ранее БД и заполняет их первоначальными данными.

Пользователи различаются по идентификатору сессии.

Точка входа в приложение: [http://localhost:8080](http://localhost:8080)

\
Логин админа: elnara
\
Пароль: 54321


\
\
**Автор: Эльнара Нурисламова, 15.10.2023**